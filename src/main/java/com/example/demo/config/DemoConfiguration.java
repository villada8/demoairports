package com.example.demo.config;

import com.example.demo.ws.client.AirportsWSClient;
import com.example.demo.ws.client.CurrencyWSClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;

/**
 * Demo Configuration
 * @author Jvillada
 * @since 1/17/2017.
 */
@Configuration
public class DemoConfiguration
{


    public static final String AIRPORT_WS_GEN_PACKAGE = "com.example.demo.ws.airport";
    public static final String COUNTRY_WS_GEN_PACKAGE = "com.example.demo.ws.country";

    @Bean
    public AirportsWSClient getAirportsClient()
    {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath(AIRPORT_WS_GEN_PACKAGE);
        WebServiceTemplate template = new WebServiceTemplate();
        template.setMarshaller(marshaller);
        template.setUnmarshaller(marshaller);
        AirportsWSClient client = new AirportsWSClient(template);
        return client;
    }

    @Bean
    public CurrencyWSClient getCountryClient()
    {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath(COUNTRY_WS_GEN_PACKAGE);
        WebServiceTemplate template = new WebServiceTemplate();
        template.setMarshaller(marshaller);
        template.setUnmarshaller(marshaller);
        CurrencyWSClient client = new CurrencyWSClient(template);
        return client;
    }
}
