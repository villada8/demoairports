package com.example.demo.config;

import com.example.demo.commons.Constants;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.instance.HazelcastInstanceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;

/**
 * Hazelcast configuration
 * @author jvillada
 * @since 1/18/2017.
 */
@Configuration
public class HazelcastCacheConfig
{
    @Bean
    public Config hazelCastConfig() {

        Config config = new Config();
        config.setInstanceName("hazelcast-cache");

        MapConfig airportsCache = new MapConfig();
        airportsCache.setTimeToLiveSeconds(20);
        airportsCache.setEvictionPolicy(EvictionPolicy.LFU);
        config.getMapConfigs().put(Constants.AIRPORTS_CACHE,airportsCache);

        return config;
    }

    @Bean
    public HazelcastInstance getInstance(@Autowired Config config)
    {
        return HazelcastInstanceFactory.newHazelcastInstance(config);
    }


}
