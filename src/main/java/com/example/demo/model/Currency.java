package com.example.demo.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * POJO that represents Currency element
 * returned by WS
 *
 * @author Jvillada
 * @since 1/17/2017.
 */
public class Currency implements Serializable
{
    private String name;

    private String code;

    public String getName()
    {
        return name;
    }

    @XmlElement(name="Currency")
    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    @XmlElement(name="CurrencyCode")
    public void setCode(String code) {
        this.code = code;
    }
}
