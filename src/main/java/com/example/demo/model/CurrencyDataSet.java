package com.example.demo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

/**
 * POJO that represents newDataSet element
 * returned by WS
 *
 * @author Jvillada
 * @since 1/17/2017.
 */
@XmlRootElement(name="NewDataSet")
@XmlAccessorType(XmlAccessType.FIELD)
public class CurrencyDataSet implements Serializable
{
    @XmlElement(name="Table")
    private List<Currency> currencies;

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
    }
}
