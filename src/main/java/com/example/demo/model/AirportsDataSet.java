package com.example.demo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

/**
 * POJO that represents newDataSet element
 * returned by WS
 *
 * @author Jvillada
 * @since 1/17/2017.
 */
@XmlRootElement(name="NewDataSet")
@XmlAccessorType(XmlAccessType.FIELD)
public class AirportsDataSet implements Serializable
{
    @XmlElement(name="Table")
    private List<Airport> airports;

    public List<Airport> getAirports() {
        return airports;
    }

    public void setAirports(List<Airport> airports)
    {
        this.airports = airports;
    }
}
