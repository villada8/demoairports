package com.example.demo.model;

import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

/**
 * Airport POJO
 *
 * @author Jvillada
 * @since 1/17/2017.
 */
public class Airport implements Serializable
{
    private String code;

    private String accessTime;

    private String country;

    private String name;

    private Currency currency;

    public String getCode() {
        return code;
    }

    @XmlElement(name="AirportCode")
    public void setCode(String code) {
        this.code = code;
    }

    public String getAccessTime() {
        return accessTime;
    }

    public void setAccessTime(String accessTime) {
        this.accessTime = accessTime;
    }

    public String getCountry() {
        return country;
    }

    @XmlElement(name="Country")
    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    @XmlElement(name="CityOrAirportName")
    public void setName(String name) {
        this.name = name;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
