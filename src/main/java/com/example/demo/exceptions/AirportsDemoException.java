package com.example.demo.exceptions;

/**
 * Generic Runtime Exception for demo application
 * @author Jvillada
 * @since 12/4/2015.
 */
public class AirportsDemoException extends RuntimeException
{
    /**
     * Constructor with error message to display
     * @param msg
     * */
    public AirportsDemoException(String msg){
        super(msg);
    }

    /**
     * Constructor with error message to display and root
     * cause exception
     * @param msg
     * @param t original exception
     * */
    public AirportsDemoException(String msg, Throwable t){
        super(msg,t);
    }
}
