package com.example.demo.web;

import com.example.demo.exceptions.AirportsDemoException;
import com.example.demo.model.Airport;
import com.example.demo.services.AirportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * Rest Controller in charge of handling airports http
 * requests
 * @author Jvillada
 * @since 1/18/2017.
 */
@RestController
@RequestMapping("/api")
public class AirportController
{

    private static final Logger log = LoggerFactory.getLogger(AirportController.class);

    @Autowired
    private AirportService service;

    /**
     * GET  /airports/:id -> get the "id" airport.
     */
    @RequestMapping(value = "/airports/{code}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Airport> getAirport(@PathVariable String code)
    {
        log.debug("REST request to get Airport : {}", code);
        Optional<Airport> opAirport = null;
        try
        {
            opAirport = service.getAirport(code);
            if(opAirport.isPresent()){
                return new ResponseEntity<>(opAirport.get(), HttpStatus.OK);
            }
            else
            {
                log.debug("Airport not found");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        }
        catch (AirportsDemoException e)
        {
            log.error("Airport Service Error ",e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
