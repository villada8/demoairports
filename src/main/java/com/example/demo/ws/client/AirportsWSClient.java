package com.example.demo.ws.client;


import com.example.demo.ws.airport.GetAirportInformationByAirportCode;
import com.example.demo.ws.airport.GetAirportInformationByAirportCodeResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

/**
 * Airports Webservice Client
 * @author jvillada
 * @since 1/17/2017.
 */
public class AirportsWSClient
{
    private static final Logger log = LoggerFactory.getLogger(AirportsWSClient.class);

    @Value("${webservice.airports.uri}")
    private String uri;

    @Value("${webservice.airports.soapAction}")
    private String soapAction;

    private WebServiceTemplate webServiceTemplate;


    public AirportsWSClient(WebServiceTemplate webServiceTemplate)
    {
        this.webServiceTemplate = webServiceTemplate;
    }

    /**
    * Method in charge of getting airport information based on code
    * @param code airport code
    * @return GetAirportInformationByAirportCodeResponse
    * */
    public GetAirportInformationByAirportCodeResponse getAirport(String code)
    {
        GetAirportInformationByAirportCode request = new GetAirportInformationByAirportCode();
        request.setAirportCode(code);

        log.info("Requesting airport {}", code);

        GetAirportInformationByAirportCodeResponse response = (GetAirportInformationByAirportCodeResponse)webServiceTemplate
                .marshalSendAndReceive(uri, request, new SoapActionCallback(soapAction));

        return response;
    }

}
