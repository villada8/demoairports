package com.example.demo.ws.client;

import com.example.demo.ws.country.GetCurrencyByCountry;
import com.example.demo.ws.country.GetCurrencyByCountryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

/**
 * Currency Webservice Client
 * @author jvillada
 * @since 1/17/2017.
 */
public class CurrencyWSClient
{
    private static final Logger log = LoggerFactory.getLogger(CurrencyWSClient.class);

    @Value("${webservice.country.uri}")
    private String uri;

    @Value("${webservice.country.soapAction}")
    private String soapAction;

    private WebServiceTemplate webServiceTemplate;

    public CurrencyWSClient(WebServiceTemplate webServiceTemplate)
    {
        this.webServiceTemplate = webServiceTemplate;
    }

    /**
     * Method in charge of getting currency for a country
     * @param country country name
     * @return GetCurrencyByCountryResponse
     * */
    public GetCurrencyByCountryResponse getCurrencyByCountry(String country)
    {
        GetCurrencyByCountry request = new GetCurrencyByCountry();
        request.setCountryName(country);

        log.debug("Requesting Currency {}", country);

        GetCurrencyByCountryResponse response = (GetCurrencyByCountryResponse)webServiceTemplate
                .marshalSendAndReceive(uri, request, new SoapActionCallback(soapAction));

        return response;
    }

}
