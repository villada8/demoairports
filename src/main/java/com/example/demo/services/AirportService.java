package com.example.demo.services;

import com.example.demo.commons.Constants;
import com.example.demo.exceptions.AirportsDemoException;
import com.example.demo.model.Airport;
import com.example.demo.model.AirportsDataSet;
import com.example.demo.model.Currency;
import com.example.demo.model.CurrencyDataSet;
import com.example.demo.ws.airport.GetAirportInformationByAirportCodeResponse;
import com.example.demo.ws.client.AirportsWSClient;
import com.example.demo.ws.client.CurrencyWSClient;
import com.example.demo.ws.country.GetCurrencyByCountryResponse;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Service responsible of retrieving airport information and
 * correspondent country currency information
 * @author jvillada
 * @since 1/17/2017.
 */
@Service
public class AirportService
{

    private static final Logger log = LoggerFactory.getLogger(AirportService.class);

    @Autowired
    private AirportsWSClient airportsWSClient;

    @Autowired
    private CurrencyWSClient currencyWSClient;

    @Autowired
    private HazelcastInstance hazelcastInstance;


    /**
     * Get airport information with correspondent currency data
     * @param code airport code
     * @return Optional<>Airport</>
     * */
    public Optional<Airport> getAirport(String code)
    {

        log.info("Getting airport using code {}",code);

        IMap airportscache = hazelcastInstance.getMap(Constants.AIRPORTS_CACHE);


        Object cachedAirport = airportscache.get(code);
        if(cachedAirport != null)
        {
            log.debug("Airport {} found in cache",code);
            return Optional.of((Airport)cachedAirport);
        }




        try
        {
            log.debug("Accessing Airports WS.....");
            GetAirportInformationByAirportCodeResponse airport = airportsWSClient.getAirport(code);
            log.debug("Response received from Airports WS.....{}",airport);
            AirportsDataSet dataSet = (AirportsDataSet)convertToPojo(airport.getGetAirportInformationByAirportCodeResult(), AirportsDataSet.class);

            List<Airport> airports = dataSet.getAirports();
            if (airports != null)
            {
                Airport firstAirport = airports.get(0);
                log.debug("Accessing Countries WS.....");
                GetCurrencyByCountryResponse currencyResp = currencyWSClient.getCurrencyByCountry(firstAirport.getCountry());
                log.debug("Response received from Countries WS.....{}",airport);
                CurrencyDataSet currencyDataSet = (CurrencyDataSet)convertToPojo(currencyResp.getGetCurrencyByCountryResult(), CurrencyDataSet.class);
                List<Currency> currencies = currencyDataSet.getCurrencies();
                if (currencies != null)
                {
                    Currency currency = currencies.get(0);
                    firstAirport.setCurrency(currency);
                    LocalDateTime localDateTime = LocalDateTime.now();
                    firstAirport.setAccessTime(localDateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
                    log.debug("Storing airport info in cache");
                    airportscache.put(code,firstAirport);
                    log.info("Airport Found {}",firstAirport.toString());
                    return Optional.of(firstAirport) ;
                }
            }
        }
        catch (Exception e)
        {
            log.error("Error getting airport",e);
            throw new AirportsDemoException("Error Getting airport",e);
        }


        log.info("Airport not found using code {}",code);
        return Optional.empty();
    }

    private Object convertToPojo(String source, Class targetClass) throws JAXBException
    {
        log.debug("JAXB Conversion from {} to POJO {}",source,targetClass.getName());
        JAXBContext jaxbContext = JAXBContext.newInstance(targetClass);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return jaxbUnmarshaller.unmarshal(new StringReader(source));
    }



}
