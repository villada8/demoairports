package com.example.demo.services;

import com.example.demo.commons.Constants;
import com.example.demo.model.Airport;
import com.example.demo.ws.airport.GetAirportInformationByAirportCodeResponse;
import com.example.demo.ws.client.AirportsWSClient;
import com.example.demo.ws.client.CurrencyWSClient;
import com.example.demo.ws.country.GetCurrencyByCountryResponse;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ws.client.core.WebServiceTemplate;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

/**
 * Created by jvillada on 1/18/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AirportServiceTest.ContextConfiguration.class})
public class AirportServiceTest
{
    @Autowired
    private AirportService service;

    @Autowired
    private AirportsWSClient airportsWSClient;

    @Autowired
    private CurrencyWSClient currencyWSClient;

    @Autowired
    private HazelcastInstance hazelcastInstance;



    @Test
    public void testGetAirport() throws JAXBException, IOException, URISyntaxException {

        GetAirportInformationByAirportCodeResponse rsp = Mockito.mock(GetAirportInformationByAirportCodeResponse.class);
        GetCurrencyByCountryResponse countryRsp = Mockito.mock(GetCurrencyByCountryResponse.class);

        URL resource = this.getClass().getResource("/airportResponse.xml");
        URL curResource = this.getClass().getResource("/currencyResponse.xml");


        Mockito.when(hazelcastInstance.getMap(Constants.AIRPORTS_CACHE)).thenReturn(Mockito.mock(IMap.class));
        Mockito.when(airportsWSClient.getAirport("FLL")).thenReturn(rsp);
        Mockito.when(currencyWSClient.getCurrencyByCountry(Mockito.anyString())).thenReturn(countryRsp);
        Mockito.when(rsp.getGetAirportInformationByAirportCodeResult()).thenReturn(new String(Files.readAllBytes(Paths.get(resource.toURI()))));
        Mockito.when(countryRsp.getGetCurrencyByCountryResult()).thenReturn(new String(Files.readAllBytes(Paths.get(curResource.toURI()))));

        Optional<Airport> airport = service.getAirport("FLL");

        Assert.assertNotNull(airport.get().getCurrency());
        Assert.assertNotNull(airport.get().getAccessTime());
        Assert.assertEquals(airport.get().getName(),"FT. LAUDERDALE INTL");
    }

    @Configuration
    public static class ContextConfiguration {

        @Bean
        public AirportService getService()
        {
            return new AirportService();
        }

        @Bean
        public HazelcastInstance getInstance()
        {
            return Mockito.mock(HazelcastInstance.class);
        }

        @Bean
        public WebServiceTemplate getTemplate()
        {
            return Mockito.mock(WebServiceTemplate.class);
        }

        @Bean
        public AirportsWSClient getAirportClient()
        {
            return Mockito.mock(AirportsWSClient.class);
        }

        @Bean
        public CurrencyWSClient getCurrencyClient()
        {
            return Mockito.mock(CurrencyWSClient.class);
        }
    }

}
